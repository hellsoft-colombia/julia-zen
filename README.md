# Julia Zen
Backend service for Julia
- Fastify + Typescript
- Docker & Docker-compose

# Run local environment without docker
To run the project without using docker you should run two commands to enable hot-reload feature.

First run the build command in background or in another terminal using:
> yarn buid:dev

This will recompile any new file you change since the last time, then you should run the server using:
> yarn dev

# Run local environment using docker
To run the local environment using docker you can use:
> `$ docker-compose up`

# Run Just DB
If you want to run only the database, you can run it using:
> `$ docker-compose -f docker-compose-db.yml up`

it will run the container exposing it throught the default port.

# TroubleShooting 
- **Error to create database**: This error could be originated because the `julia-db` folder have any corroupted data, to fix that you can delete this folder and run again the docker command

# Credits
This project was bootstraped from [this template](https://github.com/mehmetsefabalik/fastify-template)
