db.createUser(
    {
        user: "testUser",
        pwd: "testPasswd",
        roles: [
            {
                role: "readWrite",
                db : "julia-db"
            }
        ]
    }
)
