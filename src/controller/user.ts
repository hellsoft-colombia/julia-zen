import { FastifyReply, FastifyRequest, RouteHandlerMethod } from "fastify";
import { UserDao } from "../dao";

const userRequestHandler: RouteHandlerMethod = async (
  request: FastifyRequest,
  reply: FastifyReply
) => {
  request.log.info("Adding a user to the db..");
  const user = await UserDao.createUser(request.body);
  reply.status(201).send(user);
};

export const UserController = {
  userRequestHandler,
};
