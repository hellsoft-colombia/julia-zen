import {
  FastifyReply,
  FastifyRequest,
  RouteHandlerMethod,
  RouteShorthandOptions,
} from "fastify";
import { FastifyServer } from "../interface/server";

const healthResponseOptions: RouteShorthandOptions = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          message: {
            type: "string",
          },
        },
      },
    },
  },
};

const healthRequestHandler: RouteHandlerMethod = async (
  _request: FastifyRequest,
  reply: FastifyReply
) => {
  reply.code(200).send({ message: "running" });
};

const readyResponseOptions: RouteShorthandOptions = {
  schema: {
    response: {
      200: {
        type: "object",
        properties: {
          message: {
            type: "string",
          },
        },
      },
    },
  },
};

const readyRequestHandler = async (
  reply: FastifyReply,
  server: FastifyServer
): Promise<void> => {
  if (server.upAndRunning) {
    reply.code(200).send({ message: "up and running" });
    return;
  }

  reply.code(400).send({ message: "not yet" });
};

export const HealthController = {
  healthResponseOptions,
  healthRequestHandler,
  readyResponseOptions,
  readyRequestHandler,
};
