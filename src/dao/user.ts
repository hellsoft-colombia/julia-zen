import User from "../models/user-model";

export const createUser = async (data) => {
  return await User.create(data);
};
