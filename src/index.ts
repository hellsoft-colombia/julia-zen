import { getServer, start } from "./app";

const server = getServer();

start(server);
