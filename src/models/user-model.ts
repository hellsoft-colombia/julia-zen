import * as mongoose from "mongoose";

export interface UserType extends mongoose.Document {
  _id: string;
  name: string;
  email: string;
}

export type UserModel = UserType;

const UserSchema = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: false,
      index: true,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
    collection: "user",
  }
);

export default mongoose.model<UserModel>("User", UserSchema);
