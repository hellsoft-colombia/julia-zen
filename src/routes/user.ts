import { FastifyServer } from "../interface/server";
import { UserController } from "../controller/user";
import { UserPaths } from "../constants";

export const userRoutes = (server: FastifyServer): void => {
  server.post(UserPaths.USER_RESOURCE_PATH, UserController.userRequestHandler);
};
