import { FastifyServer } from "../interface/server";
import { HealthController } from "../controller/health-check";
import { FastifyReply, FastifyRequest } from "fastify";
import { ServerHealthPaths } from "../constants";

export const healthCheck = (server: FastifyServer): void => {
  server.get(
    ServerHealthPaths.HEALTH_RESOURCE_PATH,
    HealthController.healthResponseOptions,
    HealthController.healthRequestHandler
  );

  server.get(
    ServerHealthPaths.READY_RESOURCE_PATH,
    HealthController.readyResponseOptions,
    async (_request: FastifyRequest, reply: FastifyReply) =>
      HealthController.readyRequestHandler(reply, server)
  );
};
