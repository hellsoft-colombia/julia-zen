// Server Health Resource Paths
const HEALTH_RESOURCE_PATH = "/health";
const READY_RESOURCE_PATH = "/ready";

// User Resource Paths
const USER_RESOURCE_PATH = "/user";

export const ServerHealthPaths = {
  HEALTH_RESOURCE_PATH,
  READY_RESOURCE_PATH,
};

export const UserPaths = {
  USER_RESOURCE_PATH,
};
