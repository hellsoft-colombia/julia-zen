import fastifyEnv from "fastify-env";
import fastifyStatic from "fastify-static";
import path from "path";
import { getOptions } from "./config";
import { FastifyServer } from "./interface/server";
import { Route } from "./interface/route";
import { HealhCheckRoute, UserRoute } from "./routes";
import { Db } from "./constants";
import fastify from "fastify";
import mongoose from "mongoose";

const createServer = (fastify: any): FastifyServer => {
  const config: any = {};
  if (process.env.NODE_ENV !== "test") {
    config.logger = true;
  }
  return fastify(config);
};

const registerPlugins = async (server: FastifyServer) => {
  await server.register(fastifyEnv, getOptions());
  await server.register(fastifyStatic, {
    root: path.join(__dirname, "../client"),
    serve: false,
  });
};

const registerRoutes = (server: FastifyServer) => {
  const routes: Array<Route> = [
    HealhCheckRoute.healthCheck,
    UserRoute.userRoutes,
  ];
  routes.forEach((route) => route(server));
};

const run = (server: FastifyServer) => {
  mongoose.connect(
    Db.MONGODB_URL,
    { useNewUrlParser: true, useUnifiedTopology: true },
    (err) => {
      if (!err) console.log("😎 MongoDB connection successful.");
      else
        console.log(
          "❌ Error in DB connection : " + JSON.stringify(err, undefined, 2)
        );
    }
  );

  server.listen(Number(process.env.APP_PORT), "0.0.0.0", (err: any) => {
    if (err) {
      server.log.error(err);
      process.exit(1);
    }
  });
};

export function getServer(): FastifyServer {
  return createServer(fastify);
}

export async function start(server: FastifyServer): Promise<void> {
  registerRoutes(server);

  await registerPlugins(server);

  server.decorate("upAndRunning", true);

  await server.ready();

  if (process.env.NODE_ENV !== "test") {
    run(server);
  }
}
