FROM node:15.7-alpine as builder

ENV LANG C.UTF-8
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
COPY package.json /app/package.json
RUN yarn install --silent
COPY . /app
RUN yarn build

FROM node:15.7-alpine

WORKDIR /usr/src/app

COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/package.json ./package.json

EXPOSE 3000

CMD ["npm", "run", "start"]
